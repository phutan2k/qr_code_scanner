package com.lephutan.qr_code_scanner.db

import com.lephutan.qr_code_scanner.db.database.QrResultDataBase
import com.lephutan.qr_code_scanner.db.entities.QrResult
import java.util.*

class DBHelper(var qrResultDataBase: QrResultDataBase) : DBHelperI {
    override suspend fun insertQrResult(result: String): Int {
        val time = Calendar.getInstance()
        val resultType = "TEXT"
        val qrResult =
            QrResult(result = result, resultType = resultType, calender = time, favourite = false)

        return qrResultDataBase.getQrDao().insertQrResult(qrResult).toInt()
    }

    override suspend fun getQrResultId(id: Int): QrResult {
        return qrResultDataBase.getQrDao().getQrResult(id)
    }

    override suspend fun addToFavourite(id: Int): Int {
        return qrResultDataBase.getQrDao().addToFavourite(id)
    }

    override suspend fun removeFromFavorite(id: Int): Int {
        return qrResultDataBase.getQrDao().removeFromFavourite(id)
    }

    override suspend fun getAllQrScannedResult(): List<QrResult> {
        return qrResultDataBase.getQrDao().getAllScannedResult()
    }

    override suspend fun getAllFavouriteQrScannedResult(): List<QrResult> {
        return qrResultDataBase.getQrDao().getAllFavouriteResult()
    }

    override suspend fun deleteQrResult(id: Int): Int {
        return qrResultDataBase.getQrDao().deleteQrResult(id)
    }

    override suspend fun deleteAllQRScannedResult() {
        qrResultDataBase.getQrDao().deleteAllScannedResult()
    }

    override suspend fun deleteAllFavouriteQRScannedResult() {
        qrResultDataBase.getQrDao().deleteAllFavouriteResult()
    }
}
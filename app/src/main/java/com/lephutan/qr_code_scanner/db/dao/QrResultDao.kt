package com.lephutan.qr_code_scanner.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.lephutan.qr_code_scanner.db.entities.QrResult

@Dao
interface QrResultDao {

    @Query("SELECT * FROM QrResult ORDER BY time DESC")
    suspend fun getAllScannedResult(): List<QrResult>

    @Query("SELECT * FROM QrResult WHERE favourite = 1 ORDER BY time DESC")
    suspend fun getAllFavouriteResult(): List<QrResult>

    @Query("DELETE FROM QrResult")
    suspend fun deleteAllScannedResult()

    @Query("DELETE FROM QrResult WHERE favourite = 1")
    suspend fun deleteAllFavouriteResult()

    @Query("DELETE FROM QrResult WHERE id = :id")
    suspend fun deleteQrResult(id: Int): Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertQrResult(qrResult: QrResult): Long

    @Query("SELECT * FROM QrResult WHERE id = :id")
    suspend fun getQrResult(id: Int): QrResult

    @Query("UPDATE QrResult SET favourite = 1 WHERE id = :id")
    suspend fun addToFavourite(id: Int): Int

    @Query("UPDATE QrResult SET favourite = 0 WHERE id = :id")
    suspend fun removeFromFavourite(id: Int): Int
}
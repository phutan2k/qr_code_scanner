package com.lephutan.qr_code_scanner.db

import com.lephutan.qr_code_scanner.db.entities.QrResult

interface DBHelperI {
    suspend fun insertQrResult(result: String): Int

    suspend fun getQrResultId(id: Int): QrResult

    suspend fun addToFavourite(id: Int): Int

    suspend fun removeFromFavorite(id: Int): Int

    suspend fun getAllQrScannedResult(): List<QrResult>

    suspend fun getAllFavouriteQrScannedResult(): List<QrResult>

    suspend fun deleteQrResult(id : Int) : Int

    suspend fun deleteAllQRScannedResult()

    suspend fun deleteAllFavouriteQRScannedResult()
}
package com.lephutan.qr_code_scanner.db.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.lephutan.qr_code_scanner.db.dao.QrResultDao
import com.lephutan.qr_code_scanner.db.entities.QrResult

@Database(entities = [QrResult::class], version = 1, exportSchema = false)
abstract class QrResultDataBase : RoomDatabase() {
    abstract fun getQrDao(): QrResultDao

    companion object {
        private const val DB_NAME = "QrResultDatabase"
        private var qrResultDataBase: QrResultDataBase? = null

        fun getAppDataBase(context: Context): QrResultDataBase? {
            if (qrResultDataBase == null) {
                synchronized(QrResultDataBase::class){
                    qrResultDataBase =
                        Room.databaseBuilder(
                            context.applicationContext,
                            QrResultDataBase::class.java,
                            DB_NAME
                        ).build()
                }
            }
            return qrResultDataBase!!
        }

        fun destroyInstance() {
            qrResultDataBase = null
        }
    }
}
package com.lephutan.qr_code_scanner.ui.dialog

import android.app.Dialog
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.lephutan.qr_code_scanner.R
import com.lephutan.qr_code_scanner.db.DBHelper
import com.lephutan.qr_code_scanner.db.DBHelperI
import com.lephutan.qr_code_scanner.db.database.QrResultDataBase
import com.lephutan.qr_code_scanner.db.entities.QrResult
import com.lephutan.qr_code_scanner.utils.toFormattedDisplay
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


@DelicateCoroutinesApi
class QrCodeResultDialog(var context: Context) {
    private lateinit var dialog: Dialog
    private lateinit var qrResult: QrResult
    private var onDismissListener: OnDismissListener? = null
    private lateinit var dbHelperI: DBHelperI

    private lateinit var scannedText: TextView
    private lateinit var favouriteIcon: ImageView

    init {
        init()
        initDialog()
    }

    private fun init() {
        dbHelperI = DBHelper(QrResultDataBase.getAppDataBase(context)!!)
    }

    private fun initDialog() {
        dialog = Dialog(context)
        dialog.setContentView(R.layout.layout_qr_result_show)
        dialog.setCancelable(false)

        scannedText = dialog.findViewById(R.id.tv_scanned_text)
        favouriteIcon = dialog.findViewById(R.id.iv_favourite_icon)

        onClicks()
    }

    fun setOnDismissListener(onDismissListener: OnDismissListener) {
        this.onDismissListener = onDismissListener
    }

    fun show(qrResult: QrResult) {
        this.qrResult = qrResult
        dialog.findViewById<TextView>(R.id.tv_scanned_date).text =
            qrResult.calender.toFormattedDisplay()
        scannedText.text = qrResult.result
        favouriteIcon.isSelected = qrResult.favourite

        dialog.show()
    }

    private fun onClicks() {
        favouriteIcon.setOnClickListener {
            if (favouriteIcon.isSelected) removeFromFavourite() else addToFavourite()
        }

        dialog.findViewById<ImageView>(R.id.iv_share_result).setOnClickListener {
            shareResult()
        }

        dialog.findViewById<ImageView>(R.id.iv_copy_result).setOnClickListener {
            copyResultToClipBoard()
        }

        dialog.findViewById<ImageView>(R.id.iv_cancel_dialog).setOnClickListener {
            onDismissListener?.onDismiss()
            dialog.dismiss()
        }
    }

    private fun addToFavourite() {
        favouriteIcon.isSelected = true

        GlobalScope.launch(Dispatchers.IO) {
            dbHelperI.addToFavourite(qrResult.id!!)
        }
    }

    private fun removeFromFavourite() {
        favouriteIcon.isSelected = false

        GlobalScope.launch(Dispatchers.IO) {
            dbHelperI.removeFromFavorite(qrResult.id!!)
        }
    }

    private fun shareResult() {
        val txtIntent = Intent(Intent.ACTION_SEND)
        txtIntent.type = "text/plain"
        txtIntent.putExtra(Intent.EXTRA_TEXT, scannedText.text.toString())
        context.startActivity(txtIntent)
    }

    private fun copyResultToClipBoard() {
        val clipboard = context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clip = ClipData.newPlainText("QrScannerResult", scannedText.text)
        clipboard.text = clip.getItemAt(0).text.toString()

        Toast.makeText(context, "Copied to clipboard.", Toast.LENGTH_SHORT).show()
    }

    interface OnDismissListener {
        fun onDismiss()
    }
}
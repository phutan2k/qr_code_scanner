package com.lephutan.qr_code_scanner.ui.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.zxing.Result
import com.lephutan.qr_code_scanner.R
import com.lephutan.qr_code_scanner.databinding.FragmentScanBinding
import com.lephutan.qr_code_scanner.db.DBHelper
import com.lephutan.qr_code_scanner.db.DBHelperI
import com.lephutan.qr_code_scanner.db.database.QrResultDataBase
import com.lephutan.qr_code_scanner.db.entities.QrResult
import com.lephutan.qr_code_scanner.ui.dialog.QrCodeResultDialog
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import me.dm7.barcodescanner.zxing.ZXingScannerView

@DelicateCoroutinesApi
class ScanFragment : Fragment(), ZXingScannerView.ResultHandler {
    private var _binding: FragmentScanBinding? = null
    private val binding get() = _binding!!

    private lateinit var scannerView: ZXingScannerView
    private lateinit var resultDialog: QrCodeResultDialog
    private lateinit var dbHelperI: DBHelperI

    companion object {
        fun newInstance(): ScanFragment {
            return ScanFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentScanBinding.inflate(inflater, container, false)
        init()
        initViews()
        onClicks()

        return binding.root
    }

    private fun init() {
        dbHelperI = DBHelper(QrResultDataBase.getAppDataBase(requireContext())!!)
    }

    private fun initViews() {
        initializeQrScanner()

        setResultDialog()
    }

    private fun setResultDialog() {
        resultDialog = QrCodeResultDialog(requireContext())

        resultDialog.setOnDismissListener(object : QrCodeResultDialog.OnDismissListener {
            override fun onDismiss() {
                // Reset camera
                scannerView.resumeCameraPreview(this@ScanFragment)
            }
        })
    }

    private fun onClicks() {
        binding.ivFlashToggle.setOnClickListener {
            if (it.isSelected) offFlashLight() else onFlashLight()
        }
    }

    private fun onFlashLight() {
        binding.ivFlashToggle.isSelected = true
        scannerView.flash = true
    }

    private fun offFlashLight() {
        binding.ivFlashToggle.isSelected = false
        scannerView.flash = false
    }

    private fun initializeQrScanner() {
        scannerView = ZXingScannerView(context)
        scannerView.setBackgroundColor(
            ContextCompat.getColor(
                requireContext(),
                R.color.colorTranslucent
            )
        )
        scannerView.setBorderColor(
            ContextCompat.getColor(
                requireContext(),
                R.color.colorPrimaryDark
            )
        )
        scannerView.setLaserColor(
            ContextCompat.getColor(
                requireContext(),
                R.color.colorPrimaryDark
            )
        )
        scannerView.setBorderStrokeWidth(10)
        scannerView.setAutoFocus(true)
        scannerView.setSquareViewFinder(true)
        scannerView.setResultHandler(this)

        binding.flContainerScanner.addView(scannerView)
        startQrCamera()
    }

    private fun startQrCamera() {
        scannerView.startCamera()
    }

    override fun onResume() {
        super.onResume()
        scannerView.setResultHandler(this)
        scannerView.startCamera()
    }

    override fun onPause() {
        super.onPause()
        scannerView.stopCamera()
    }

    override fun onDestroy() {
        super.onDestroy()
        scannerView.stopCamera()
        _binding = null
    }

    override fun handleResult(rawResult: Result?) {
        onQrResult(rawResult!!.text)
    }

    private fun onQrResult(text: String?) {
        if (text.isNullOrEmpty()) {
            Toast.makeText(context, getString(R.string.txt_empty_qr_code), Toast.LENGTH_SHORT)
                .show()
        } else {
            saveToDatabase(text)
        }
    }

    private fun saveToDatabase(result: String) {
        GlobalScope.launch(Dispatchers.IO) {
            try {
                val insertedRowId = dbHelperI.insertQrResult(result)
                Log.i("TanLe", "saveToDatabase: $insertedRowId")

                val qrResult = dbHelperI.getQrResultId(insertedRowId)

                GlobalScope.launch(Dispatchers.Main) {
                    resultDialog.show(qrResult)
                }
            } catch (e: Exception) {
                e.message
            }
        }
    }
}
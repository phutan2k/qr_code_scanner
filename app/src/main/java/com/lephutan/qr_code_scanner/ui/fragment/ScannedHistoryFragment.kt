package com.lephutan.qr_code_scanner.ui.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.lephutan.qr_code_scanner.R
import com.lephutan.qr_code_scanner.databinding.FragmentScannedHistoryBinding
import com.lephutan.qr_code_scanner.db.DBHelper
import com.lephutan.qr_code_scanner.db.DBHelperI
import com.lephutan.qr_code_scanner.db.database.QrResultDataBase
import com.lephutan.qr_code_scanner.db.entities.QrResult
import com.lephutan.qr_code_scanner.ui.adapter.ScannedResultListAdapter
import com.lephutan.qr_code_scanner.ui.dialog.QrCodeResultDialog
import com.lephutan.qr_code_scanner.utils.gone
import com.lephutan.qr_code_scanner.utils.visible
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

@DelicateCoroutinesApi
class ScannedHistoryFragment : Fragment() {
    private var _binding: FragmentScannedHistoryBinding? = null
    private val binding get() = _binding!!

    private lateinit var resultType: ResultListType
    private lateinit var dbHelperI: DBHelperI

    enum class ResultListType {
        ALL_RESULT, FAVOURITE_RESULT
    }

    companion object {

        private const val ARGUMENT_RESULT_LIST_TYPE = "ArgumentResultType"

        fun newInstance(screenType: ResultListType): ScannedHistoryFragment {
            val bundle = Bundle()
            bundle.putSerializable(ARGUMENT_RESULT_LIST_TYPE, screenType)

            val fragment = ScannedHistoryFragment()
            fragment.arguments = bundle

            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        handleArguments()
    }

    private fun handleArguments() {
        resultType = arguments?.getSerializable(ARGUMENT_RESULT_LIST_TYPE) as ResultListType
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentScannedHistoryBinding.inflate(inflater, container, false)
        init()
        showListOfResults()
        onClicks()

        return binding.root
    }

    private fun onClicks() {
        binding.actionbarHistory.ivRemoveAll.setOnClickListener{
            showRemoveAllScannedResultDialog()
        }
    }

    private fun showRemoveAllScannedResultDialog() {
        AlertDialog.Builder(requireContext(), R.style.CustomAlertDialog)
            .setTitle(requireContext().getString(R.string.txt_clear_all))
            .setMessage(requireContext().getString(R.string.txt_clear_all_result))
            .setPositiveButton(requireContext().getString(R.string.txt_clear)) { _, _ ->
                clearAllRecords()
            }
            .setNegativeButton(requireContext().getString(R.string.txt_cancel)) { dialog, _ ->
                dialog.cancel()
            }.show()
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun clearAllRecords() {
        GlobalScope.launch {
            when (resultType) {
                ResultListType.ALL_RESULT -> dbHelperI.deleteAllQRScannedResult()
                ResultListType.FAVOURITE_RESULT -> dbHelperI.deleteAllFavouriteQRScannedResult()
            }
        }
        binding.rvScannedHistoryRecyclerView.adapter?.notifyDataSetChanged()
        showAllResults()
    }

    private fun init() {
        dbHelperI = DBHelper(QrResultDataBase.getAppDataBase(requireContext())!!)
    }

    private fun showListOfResults() {
        when (resultType) {
            ResultListType.ALL_RESULT -> {
                showAllResults()
            }

            ResultListType.FAVOURITE_RESULT -> {
                showFavouriteResults()
            }
        }
    }

    private fun showAllResults() {
        GlobalScope.launch(Dispatchers.IO) {
            val listOfAllResult = dbHelperI.getAllQrScannedResult()

            GlobalScope.launch(Dispatchers.Main) {
                showResults(listOfAllResult)
                binding.actionbarHistory.tvHeaderText.text = getString(R.string.txt_recent_scanned)
            }
        }
    }

    private fun showFavouriteResults() {
        GlobalScope.launch(Dispatchers.IO) {
            val listOfFavouriteResult = dbHelperI.getAllFavouriteQrScannedResult()

            GlobalScope.launch(Dispatchers.Main) {
                showResults(listOfFavouriteResult)
                binding.actionbarHistory.tvHeaderText.text =
                    getString(R.string.txt_recent_favourite_results)
            }
        }
    }

    private fun showResults(listOfQrResult: List<QrResult>) {
        if (listOfQrResult.isNotEmpty()) initRecyclerView(listOfQrResult) else showEmptyState()
    }

    private fun initRecyclerView(listOfQrResult: List<QrResult>) {
        binding.rvScannedHistoryRecyclerView.layoutManager = LinearLayoutManager(context)
        val adapter =
            ScannedResultListAdapter(dbHelperI, requireContext(), listOfQrResult.toMutableList())
        binding.rvScannedHistoryRecyclerView.adapter = adapter

        showRecyclerView()
    }

    private fun showRecyclerView() {
        binding.apply {
            actionbarHistory.ivRemoveAll.visible()
            rvScannedHistoryRecyclerView.visible()
            ivNoResultFound.gone()
        }
    }

    private fun showEmptyState() {
        binding.apply {
            actionbarHistory.ivRemoveAll.gone()
            rvScannedHistoryRecyclerView.gone()
            ivNoResultFound.visible()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
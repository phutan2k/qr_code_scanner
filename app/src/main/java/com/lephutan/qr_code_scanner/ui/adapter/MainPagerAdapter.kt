package com.lephutan.qr_code_scanner.ui.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.lephutan.qr_code_scanner.ui.fragment.ScanFragment
import com.lephutan.qr_code_scanner.ui.fragment.ScannedHistoryFragment
import kotlinx.coroutines.DelicateCoroutinesApi

@DelicateCoroutinesApi
class MainPagerAdapter(var frg: FragmentManager) : FragmentStatePagerAdapter(frg) {

    override fun getCount(): Int {
        return 3
    }

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> ScanFragment.newInstance()
            1 -> ScannedHistoryFragment.newInstance(ScannedHistoryFragment.ResultListType.ALL_RESULT)
            2 -> ScannedHistoryFragment.newInstance(ScannedHistoryFragment.ResultListType.FAVOURITE_RESULT)
            else -> ScanFragment.newInstance()
        }
    }
}
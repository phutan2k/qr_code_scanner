package com.lephutan.qr_code_scanner.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.viewpager.widget.ViewPager
import com.lephutan.qr_code_scanner.R
import com.lephutan.qr_code_scanner.databinding.ActivityMainBinding
import com.lephutan.qr_code_scanner.ui.adapter.MainPagerAdapter

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setViewPagerAdapter()
        setBottomNavigation()
        setViewPagerListener()
    }

    private fun setViewPagerListener() {
        binding.viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {
                binding.bottomNavigationView.selectedItemId = when (position) {
                    0 -> R.id.scan_menu_id
                    1 -> R.id.recent_scanned_menu_id
                    2 -> R.id.favourites_menu_id
                    else -> R.id.scan_menu_id
                }
            }

            override fun onPageScrollStateChanged(state: Int) {

            }
        })
    }

    private fun setBottomNavigation() {
        binding.bottomNavigationView.setOnNavigationItemSelectedListener {
            binding.viewPager.currentItem = when (it.itemId){
                R.id.scan_menu_id -> 0
                R.id.recent_scanned_menu_id -> 1
                R.id.favourites_menu_id -> 2
                else -> 0
            }

            return@setOnNavigationItemSelectedListener true
        }
    }

    private fun setViewPagerAdapter() {
        binding.viewPager.adapter = MainPagerAdapter(supportFragmentManager)
    }
}
package com.lephutan.qr_code_scanner.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.lephutan.qr_code_scanner.R
import com.lephutan.qr_code_scanner.databinding.ItemQrResultBinding
import com.lephutan.qr_code_scanner.db.DBHelperI
import com.lephutan.qr_code_scanner.db.entities.QrResult
import com.lephutan.qr_code_scanner.ui.dialog.QrCodeResultDialog
import com.lephutan.qr_code_scanner.utils.gone
import com.lephutan.qr_code_scanner.utils.toFormattedDisplay
import com.lephutan.qr_code_scanner.utils.visible
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

@DelicateCoroutinesApi
class ScannedResultListAdapter(
    var dbHelperI: DBHelperI,
    var context: Context,
    private var listOfScannedResult: MutableList<QrResult>
) : RecyclerView.Adapter<ScannedResultListAdapter.ScannedResultListViewHolder>() {

    private var resultDialog = QrCodeResultDialog(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ScannedResultListViewHolder {
        val binding = ItemQrResultBinding.inflate(LayoutInflater.from(context), parent, false)
        return ScannedResultListViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ScannedResultListViewHolder, position: Int) {
        holder.bind(listOfScannedResult[position], position)
    }

    override fun getItemCount(): Int {
        return listOfScannedResult.size
    }

    inner class ScannedResultListViewHolder(private val binding: ItemQrResultBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(qrResult: QrResult, position: Int) {
            binding.apply {
                tvResult.text = qrResult.result
                tvTime.text = qrResult.calender.toFormattedDisplay()
            }

            setFavourite(qrResult.favourite)
            onClicks(qrResult, position)
        }

        private fun setFavourite(isFavourite: Boolean) {
            if (isFavourite) binding.ivFavouriteIcon.visible() else binding.ivFavouriteIcon.gone()
        }

        private fun onClicks(qrResult: QrResult, position: Int) {
            binding.clItemQrResult.setOnClickListener {
                resultDialog.show(qrResult)
            }

            binding.clItemQrResult.setOnLongClickListener {
                showDeleteDialog(qrResult, position)
                return@setOnLongClickListener true
            }
        }

        private fun showDeleteDialog(qrResult: QrResult, position: Int) {
            AlertDialog.Builder(context, R.style.CustomAlertDialog)
                .setTitle(context.getString(R.string.txt_delete))
                .setMessage(context.getString(R.string.txt_want_to_delete))
                .setPositiveButton(context.getString(R.string.txt_delete)) { _, _ ->
                    deleteThisRecord(qrResult, position)
                }
                .setNegativeButton(context.getString(R.string.txt_cancel)) { dialog, _ ->
                    dialog.cancel()
                }.show()
        }

        private fun deleteThisRecord(qrResult: QrResult, position: Int) {
            GlobalScope.launch(Dispatchers.IO) {
                dbHelperI.deleteQrResult(qrResult.id!!)
            }
            listOfScannedResult.removeAt(position)
            notifyItemRemoved(position)
        }
    }
}